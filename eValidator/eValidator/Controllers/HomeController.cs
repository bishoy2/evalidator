﻿using eValidator.Models;
using HL7.Dotnetcore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace eValidator.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        public ValidationEngine engine { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        public enum Settings
        {
            HL7 = 0,
            PipeDelimited = 1,
            Inpatient = 2,
            Outpatient = 3
        }

        /// <summary>
        /// This method takes in an HL7 or pipe delimited file, inpatient or outpatient, and returns the summary of what fields are missing.
        /// </summary>
        /// <param name="rawMessage"></param>
        /// <param name="messageType"></param>
        /// <param name="patientType"></param>
        /// <returns>The summary</returns>
        [HttpPost]
        public string processFile(string rawMessage, string messageType, string patientType, bool deidentify)
        {
            var messageTypeInt = int.Parse(messageType);
            var patientTypeInt = int.Parse(patientType);
            var summary = "";

            if (messageTypeInt == (int) Settings.HL7)
            {
                var msg = new Message(rawMessage);
                var isParsed = false;
                try
                {
                    isParsed = msg.ParseMessage();
                    this.engine = new ValidationEngine(msg, deidentify);
                }
                catch (Exception e)
                {
                    summary = "Valid HL7 file not supplied.";
                    return summary;
                }
                if (patientTypeInt == (int) Settings.Inpatient)
                {
                    this.engine.ValidateIPHL7();
                }
                else if (patientTypeInt == (int) Settings.Outpatient)
                {
                    this.engine.ValidateOPHL7();
                }
                summary = engine.Summary.ToString();
            }
            else if (messageTypeInt == (int) Settings.PipeDelimited)
            {
                this.engine = new ValidationEngine(rawMessage, deidentify);
                if (patientTypeInt == (int)Settings.Inpatient)
                {
                    this.engine.ValidateIPPipeDelimited();
                }
                else if (patientTypeInt == (int)Settings.Outpatient)
                {
                    this.engine.ValidateOPPipeDelimited();
                }
                summary = this.engine.Summary.ToString();
            }
            if (summary == "")
            {
                summary = "Your data is ready for integration with eValuator! Please contact a representative from Streamline Health to move along in the process.";
            }
            return summary;
        }

        [HttpPost]
        public string getCSVString(string rawMessage, string messageType, string patientType, bool deidentify)
        {
            var summary = this.processFile(rawMessage, messageType, patientType, deidentify);
            return this.engine.csv.getOutputStream();
        }
    }
}
