﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eValidator.Models
{
    public class DataElement
    {
        public string DataElementName { get; set; }
        public enum RequiredValues { R, RE }
        public RequiredValues Required { get; set; }
        public string DataElementValue { get; set; }

        public DataElement(string name, RequiredValues req, string value)
        {
            this.DataElementName = name;
            this.Required = req;
            this.DataElementValue = value;
        }
    }
}