﻿using HL7.Dotnetcore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eValidator.Models
{
    public class ValidationEngine
    {
        #region Data Members
        private Message message;
        private List<string> pipeFileInput;
        public StringBuilder Summary { get; set; }
        public OutputCSV csv { get; set; }
        private bool isHL7;
        private bool deidentify;
        #endregion

        #region Constructors
        public ValidationEngine(Message msg, bool deidentify)
        {
            this.message = msg;
            this.Summary = new StringBuilder();
            isHL7 = true;
            this.csv = new OutputCSV();
            this.deidentify = deidentify;
        }

        public ValidationEngine(string pipeFile, bool deidentify)
        {
            this.pipeFileInput = pipeFile.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToList()[0].Split('|').ToList();
            this.Summary = new StringBuilder();
            isHL7 = false;
            this.csv = new OutputCSV();
            this.deidentify = deidentify;
        }
        #endregion

        #region HL7 Validation
        public void ValidateIPHL7()
        {
            this.ValidateSharedHL7();

            this.validateDRG(this.getHL7MessageValue(DataReference.expectedDRGLocation), this.getHL7MessageValue(DataReference.expectedDRGLocation), 
                this.getHL7MessageValue(DataReference.expectedSOILocation), this.getHL7MessageValue(DataReference.expectedROMLocation));
            this.validateDiagnoses(this.message.Segments(DataReference.expectedDiagnosesLocation).Select(d => d.Value).ToList(), false);
        }

        public void ValidateOPHL7()
        {
            this.ValidateSharedHL7();

            this.validateOPCharges(this.message.Segments(DataReference.expectedOPChargesLocation).Select(d => d.Value).ToList());
            this.validateOPDX(this.message.Segments(DataReference.expectedDiagnosesLocation).Select(d => d.Value).ToList());
        }

        private void ValidateSharedHL7()
        {
            this.validateFacility(this.getHL7MessageValue(DataReference.expectedFacilityLocation));
            this.validateAccount(this.getHL7MessageValue(DataReference.expectedAccountLocation));
            this.validateMRN(this.getHL7MessageValue(DataReference.expectedMRNLocation));
            this.validatePatientName(this.getHL7MessageValue(DataReference.expectedPatientNameLocation));
            this.validateAdmitDate(this.getHL7MessageValue(DataReference.expectedAdmitDateLocation));
            this.validateDischargeDate(this.getHL7MessageValue(DataReference.expectedDischargeDateLocation));
            this.validateDISCHDISP(this.getHL7MessageValue(DataReference.expectedDISCHDISPLocation));
            this.validateDOB(this.getHL7MessageValue(DataReference.expectedDOBLocation));
            this.validateSex(this.getHL7MessageValue(DataReference.expectedSexLocation));
            this.validateService(this.getHL7MessageValue(DataReference.expectedServiceLocation));
            this.validatePayor(this.getHL7MessageValue(DataReference.expectedPayorLocation));
            this.validateCharges(this.getHL7MessageValue(DataReference.expectedTotalChargesLocation));
            this.validateAttendingName(this.getHL7MessageValue(DataReference.expectedAttendingNameLocation));
            this.validateCoder(this.getHL7MessageValue(DataReference.expectedCoderLocation));
            this.validateProcedures(this.message.Segments(DataReference.expectedProceduresLocation).Select(d => d.Value).ToList());
        }
        #endregion

        #region Pipe Delimited Validation
        public void ValidateIPPipeDelimited()
        {
            try
            {
                this.validateFacility(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.Facility]);
                this.validateSharedPipeDelimited();

                var msdrg = this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.MSDRG];
                var aprdrg = this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.APRDRG];
                var soi = this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.SOI];
                var rom = this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.ROM];

                this.validateDRG(msdrg, aprdrg, soi, rom);
                this.validateDiagAndProcCntIP();
                this.validatePipeIPDiagnoses(int.Parse(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.DIAGCNT]));
                this.validatePipeIPProcedures(int.Parse(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.PROCCNT]), int.Parse(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.DIAGCNT]));
            }
            catch (Exception e)
            {
                this.Summary.Append("There was an error processing your pipe delimited file. Please check its format against the official Streamline Health Pipe Delimited specification.");
            }
        }
        public void ValidateOPPipeDelimited()
        {
            try { 
                this.validateSharedPipeDelimited();

                this.validatePipeOPCharges();
                this.validateDiagAndProcCntOP();
                this.validatePipeOPDiagnoses(int.Parse(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.DIAGCNT]));
                this.validatePipeOPProcedures(int.Parse(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.PROCCNT]), int.Parse(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.DIAGCNT]));
            }
            catch (Exception e)
            {
                this.Summary.Append("There was an error processing your pipe delimited file. Please check its format against the official Streamline Health Pipe Delimited specification.");
            }
        }

        private void validateSharedPipeDelimited()
        {
            this.validateAccount(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.Account]);
            this.validateMRN(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.MRN]);
            this.validatePatientName(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.PatientName]);
            this.validateAdmitDate(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.Admit]);
            this.validateDischargeDate(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.Discharge]);
            this.validateDISCHDISP(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.DischDisp]);
            this.validateDOB(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.DOB]);
            this.validateSex(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.Sex]);
            this.validateService(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.Service]);
            this.validatePayor(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.Payor]);
            this.validateCharges(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.Charges]);
            this.validateAttendingName(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.AttendingName]);
            this.validateCoder(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.Coder]);
        }
        
        #endregion

        private string getHL7MessageValue(string field)
        {
            var fieldValue = "";
            try
            {
                fieldValue = this.message.GetValue(field);
            }
            catch(HL7Exception h)
            {
                //fieldValue stays as an empty string
            }
            return fieldValue;
        }

        private void validateFacility(string facility)
        {
            var notPresent = string.IsNullOrEmpty(facility);
            if (notPresent)
            {
                this.Summary.Append("FACILITY").Append(DataReference.RFieldMessage).Append(DataReference.alphaNumericFormat);
            }
            if (this.deidentify && !notPresent)
            {
                facility = DataReference.dummyFacility;
            }
            this.csv.addRow("Facility", !notPresent, "R", isHL7 ? DataReference.expectedFacilityLocation : ((int)DataReference.InpatientPipeDataLocations.Facility+1).ToString(), facility);
        }

        private void validateAccount(string account)
        {
            var notPresent = string.IsNullOrEmpty(account);
            if (notPresent)
            {
                this.Summary.Append("ACCOUNT").Append(DataReference.RFieldMessage).Append(DataReference.alphaNumericFormat);
            }
            if (this.deidentify && !notPresent)
            {
                account = DataReference.dummyAccount;
            }
            this.csv.addRow("Account", !notPresent, "R", isHL7 ? DataReference.expectedAccountLocation : ((int)DataReference.InpatientPipeDataLocations.Account + 1).ToString(), account);
        }

        private void validateMRN(string mrn)
        {
            var notPresent = string.IsNullOrEmpty(mrn);
            if (notPresent)
            {
                this.Summary.Append("MRN").Append(DataReference.RFieldMessage).Append(DataReference.alphaNumericFormat);
            }
            if (this.deidentify && !notPresent)
            {
                mrn = DataReference.dummyMRN;
            }
            this.csv.addRow("MRN", !notPresent, "R", isHL7 ? DataReference.expectedMRNLocation : ((int)DataReference.InpatientPipeDataLocations.MRN + 1).ToString(), mrn);
        }

        private void validatePatientName(string patName)
        {
            var notPresent = string.IsNullOrEmpty(patName);
            if (notPresent)
            {
                this.Summary.Append("PATIENT NAME").Append(DataReference.REFieldMessage).Append(DataReference.nameFormat);
            }
            if (this.deidentify && !notPresent)
            {
                patName = DataReference.dummyPatientName;
            }
            this.csv.addRow("Patient Name", !notPresent, "RE", isHL7 ? DataReference.expectedPatientNameLocation : ((int)DataReference.InpatientPipeDataLocations.PatientName + 1).ToString(), patName);
        }

        private void validateAdmitDate(string admit)
        {
            var notPresent = string.IsNullOrEmpty(admit);
            if (notPresent)
            {
                this.Summary.Append("ADMIT Date").Append(DataReference.RFieldMessage).Append(DataReference.dateFormat);
            }
            if (this.deidentify && !notPresent)
            {
                admit = DataReference.dummyDate;
            }
            this.csv.addRow("Admit Date", !notPresent, "R", isHL7 ? DataReference.expectedAdmitDateLocation : ((int)DataReference.InpatientPipeDataLocations.Admit + 1).ToString(), admit);
        }

        private void validateDischargeDate(string discharge)
        {
            var notPresent = string.IsNullOrEmpty(discharge);
            if (notPresent)
            {
                this.Summary.Append("DISCHARGE date").Append(DataReference.REFieldMessage).Append(DataReference.dateFormat);
            }
            if (this.deidentify && !notPresent)
            {
                discharge = DataReference.dummyDate;
            }
            this.csv.addRow("Discharge Date", !notPresent, "RE", isHL7 ? DataReference.expectedDischargeDateLocation : ((int)DataReference.InpatientPipeDataLocations.Discharge + 1).ToString(), discharge);
        }

        private void validateDISCHDISP(string dischdisp)
        {
            var notPresent = string.IsNullOrEmpty(dischdisp);
            if (notPresent)
            {
                this.Summary.Append("DISCHARGE DISPOSITION").Append(DataReference.REFieldMessage).Append(DataReference.twoDigitFormat);
            }
            this.csv.addRow("Discharge Disposition", !notPresent, "RE", isHL7 ? DataReference.expectedDISCHDISPLocation : ((int)DataReference.InpatientPipeDataLocations.DischDisp + 1).ToString(), dischdisp);
        }

        private void validateDOB(string dob)
        {
            var notPresent = string.IsNullOrEmpty(dob);
            if (notPresent)
            {
                this.Summary.Append("Patient DOB").Append(DataReference.REFieldMessage).Append(DataReference.dateFormat);
            }
            if (this.deidentify && !notPresent)
            {
                dob = DataReference.dummyDate;
            }
            this.csv.addRow("DOB", !notPresent, "RE", isHL7 ? DataReference.expectedDOBLocation : ((int)DataReference.InpatientPipeDataLocations.DOB + 1).ToString(), dob);
        }

        private void validateSex(string sex)
        {
            var notPresent = string.IsNullOrEmpty(sex);
            if (notPresent)
            {
                this.Summary.Append("Patient SEX").Append(DataReference.REFieldMessage).Append(DataReference.sexFormat);
            }
            this.csv.addRow("SEX", !notPresent, "RE", isHL7 ? DataReference.expectedSexLocation : ((int)DataReference.InpatientPipeDataLocations.Sex + 1).ToString(), sex);
        }

        private void validateService(string svc)
        {
            var notPresent = string.IsNullOrEmpty(svc);
            if (notPresent)
            {
                this.Summary.Append("SERVICE").Append(DataReference.RFieldMessage).Append(DataReference.alphaNumericFormat);
            }
            this.csv.addRow("Service", !notPresent, "R", isHL7 ? DataReference.expectedServiceLocation : ((int)DataReference.InpatientPipeDataLocations.Service + 1).ToString(), svc);
        }

        private void validatePayor(string payor)
        {
            var notPresent = string.IsNullOrEmpty(payor);
            if (notPresent)
            {
                this.Summary.Append("PAYOR").Append(DataReference.RFieldMessage).Append(DataReference.alphaNumericFormat);
            }
            this.csv.addRow("Payor", !notPresent, "R", isHL7 ? DataReference.expectedPayorLocation : ((int)DataReference.InpatientPipeDataLocations.Payor + 1).ToString(), payor);
        }

        private void validateCharges(string charges)
        {
            var notPresent = string.IsNullOrEmpty(charges);
            if (notPresent)
            {
                this.Summary.Append("TOTAL CHARGES").Append(DataReference.REFieldMessage).Append(DataReference.moneyFormat);
            }
            this.csv.addRow("Total Charges", !notPresent, "RE", isHL7 ? DataReference.expectedTotalChargesLocation : ((int)DataReference.InpatientPipeDataLocations.Charges + 1).ToString(), charges);
        }

        private void validateAttendingName(string attendingName)
        {
            var notPresent = string.IsNullOrEmpty(attendingName);
            if (notPresent)
            {
                this.Summary.Append("ATTENDING NAME").Append(DataReference.REFieldMessage).Append(DataReference.nameFormat);
            }
            this.csv.addRow("Attending Name", !notPresent, "RE", isHL7 ? DataReference.expectedAttendingNameLocation : ((int)DataReference.InpatientPipeDataLocations.AttendingName + 1).ToString(), attendingName);
        }

        private void validateCoder(string coder)
        {
            var notPresent = string.IsNullOrEmpty(coder);
            if (notPresent)
            {
                this.Summary.Append("CODER").Append(DataReference.REFieldMessage).Append(DataReference.nameFormat);
            }
            this.csv.addRow("Coder", !notPresent, "RE", isHL7 ? DataReference.expectedCoderLocation : ((int)DataReference.InpatientPipeDataLocations.Coder + 1).ToString(), coder);
        }

        private void validateDRG(string msdrg, string aprdrg, string soi, string rom)
        {
            var notPresentMSDRG = string.IsNullOrEmpty(msdrg);
            var notPresentAPRDRG = string.IsNullOrEmpty(aprdrg);
            var notPresentROM = string.IsNullOrEmpty(rom);
            var notPresentSOI = string.IsNullOrEmpty(soi);

            if (notPresentMSDRG && notPresentAPRDRG)
            {
                this.Summary.Append("MSDRG is missing; if using MSDRG, please include in the following format: ").Append(DataReference.alphaNumericWithSpecialCharFormat);
            }
            if (notPresentAPRDRG && notPresentMSDRG)
            {
                this.Summary.Append("APRDRG is missing; if using APRDRG, please include in the following format: ").Append(DataReference.alphaNumericWithSpecialCharFormat);
            }
            if (notPresentROM && notPresentMSDRG)
            {
                this.Summary.Append("ROM").Append(DataReference.RFieldMessage).Append(DataReference.singleNumericFormat);
            }
            if (notPresentSOI && notPresentMSDRG)
            {
                this.Summary.Append("SOI").Append(DataReference.RFieldMessage).Append(DataReference.singleNumericFormat);
            }
            
            this.csv.addRow("MSDRG", !notPresentMSDRG, "R if MSDRG is used", isHL7 ? DataReference.expectedDRGLocation : ((int)DataReference.InpatientPipeDataLocations.MSDRG + 1).ToString(), msdrg);
            this.csv.addRow("APRDRG", !notPresentAPRDRG, "R if APRDRG is used", isHL7 ? DataReference.expectedDRGLocation : ((int)DataReference.InpatientPipeDataLocations.APRDRG + 1).ToString(), aprdrg);
            this.csv.addRow("ROM", !notPresentROM, "R if APRDRG is used", isHL7 ? DataReference.expectedROMLocation : ((int)DataReference.InpatientPipeDataLocations.ROM + 1).ToString(), rom);
            this.csv.addRow("SOI", !notPresentSOI, "R if APRDRG is used", isHL7 ? DataReference.expectedSOILocation : ((int)DataReference.InpatientPipeDataLocations.SOI + 1).ToString(), soi);

        }

        private void validateDiagnoses(List<string> diagnoses, bool isOutpatient)
        {
            if (diagnoses.Count == 0)
            {
                this.Summary.Append("No Diagnoses found, please include. ");
                return;
            }
            foreach (var diag in diagnoses)
            {
                var i = diagnoses.IndexOf(diag) + 1;
                var dx = diag.Split('|')[3];
                var poa = diag.Split('|')[10];

                var notPresentDX = string.IsNullOrEmpty(dx);
                var notPresentPOA = string.IsNullOrEmpty(poa);

                if (notPresentDX)
                {
                    this.Summary.Append("Empty DX").Append(i).Append(" field found. ");
                }
                if (notPresentPOA && !isOutpatient)
                {
                    this.Summary.Append("Empty POA").Append(i).Append(" field found, if the POA is available for this DX please include. ");
                }

                this.csv.addRow("DX" + i, !notPresentDX, "R", DataReference.expectedDiagnosesLocation, dx);
                if (!isOutpatient)
                {
                    this.csv.addRow("POA" + i, !notPresentPOA, "RE", "DG1-10", poa);
                }
            }
        }

        private void validateProcedures(List<string> procedures)
        {
            if (procedures.Count == 0)
            {
                this.Summary.Append("No Procedures found, if available please include. ");
                return;
            }
            foreach (var proc in procedures)
            {
                var i = procedures.IndexOf(proc) + 1;
                var px = proc.Split('|')[3];
                var procDate = proc.Split('|')[5];
                var procProvider = proc.Split('|')[11];

                var notPresentPX = string.IsNullOrEmpty(px);
                var notPresentProcDate = string.IsNullOrEmpty(procDate);
                var notPresentProcProvider = string.IsNullOrEmpty(procProvider);

                if (notPresentPX)
                {
                    this.Summary.Append("Empty PX").Append(i).Append(" field found, if available please include. ");
                }
                if (notPresentProcDate)
                {
                    this.Summary.Append("Empty Procedure Date (Row ").Append(i).Append(") field found, if the Procedure Date is available for this Procedure please include. ");
                }
                if (notPresentProcProvider)
                {
                    this.Summary.Append("Empty Procedure Provider (Row ").Append(i).Append(") field found, if the Procedure Provider is available for this Procedure please include. ");
                }

                this.csv.addRow("PX" + i, !notPresentPX, "RE", "PR1-3.1", px);
                this.csv.addRow("Procedure Date", !notPresentProcDate, "RE", "PR1-5", procDate);
                this.csv.addRow("Procedure Provider", !notPresentProcProvider, "RE", "PR1-11", procProvider);
            }
        }

        private void validateOPCharges(List<string> ftsegment)
        {
            if (ftsegment.Count == 0)
            {
                this.Summary.Append("No Charges found, please include.");
                return;
            }
            var i = 0;
            foreach(var charge in ftsegment)
            {
                var chargeCode = charge.Split('|')[7];
                var chargesAmount = charge.Split('|')[11];

                var notPresentChargeCode = string.IsNullOrEmpty(chargeCode);
                var notPresentChargeAmount = string.IsNullOrEmpty(chargesAmount);

                if (notPresentChargeCode)
                {
                    this.Summary.Append("Charge Code ").Append(i).Append(DataReference.RFieldNoFormatMessage);
                }
                if (notPresentChargeAmount)
                {
                    this.Summary.Append("Charges Amount ").Append(i).Append(DataReference.RFieldNoFormatMessage);
                }

                this.csv.addRow("Charge Code " + i, !notPresentChargeCode, "R", "FT1-7", chargeCode);
                this.csv.addRow("Charge Amount " + i, !notPresentChargeAmount, "R", "FT1-11", chargesAmount);

                i++;
            }
        }

        private void validateOPDX(List<string> dx)
        {
            if (dx.Count == 0)
            {
                this.Summary.Append("Outpatient data must have a primary diagnosis. ");
                return;
            }
            validateDiagnoses(dx, true);
        }

        private void validateDiagAndProcCntIP()
        {
            var notPresentDiagCnt = string.IsNullOrEmpty(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.DIAGCNT]);
            var notPresentProcCnt = string.IsNullOrEmpty(this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.PROCCNT]);

            if (notPresentDiagCnt)
            {
                this.Summary.Append("DIAGCNT").Append(DataReference.RFieldMessage).Append(DataReference.numberFormat);
            }
            if (notPresentProcCnt)
            {
                this.Summary.Append("PROCCNT").Append(DataReference.RFieldMessage).Append(DataReference.numberFormat);
            }

            this.csv.addRow("DIAGCNT", !notPresentDiagCnt, "R", ((int)DataReference.InpatientPipeDataLocations.DIAGCNT + 1).ToString(), this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.DIAGCNT]);
            this.csv.addRow("PROCCNT", !notPresentProcCnt, "R", ((int)DataReference.InpatientPipeDataLocations.PROCCNT + 1).ToString(), this.pipeFileInput[(int)DataReference.InpatientPipeDataLocations.PROCCNT]);
        }

        private void validateDiagAndProcCntOP()
        {
            var notPresentDiagCnt = string.IsNullOrEmpty(this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.DIAGCNT]);
            var notPresentProcCnt = string.IsNullOrEmpty(this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.PROCCNT]);

            if (notPresentDiagCnt)
            {
                this.Summary.Append("DIAGCNT").Append(DataReference.RFieldMessage).Append(DataReference.numberFormat);
            }
            if (notPresentProcCnt)
            {
                this.Summary.Append("PROCCNT").Append(DataReference.RFieldMessage).Append(DataReference.numberFormat);
            }

            this.csv.addRow("DIAGCNT", !notPresentDiagCnt, "R", ((int)DataReference.OutpatientPipeDataLocations.DIAGCNT + 1).ToString(), this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.DIAGCNT]);
            this.csv.addRow("PROCCNT", !notPresentProcCnt, "R", ((int)DataReference.OutpatientPipeDataLocations.PROCCNT + 1).ToString(), this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.PROCCNT]);
        }

        private void validatePipeIPDiagnoses(int diagcnt)
        {
            var startingIndex = ((int)DataReference.InpatientPipeDataLocations.DIAGCNT) + 2;
            if (startingIndex == -1)
            {
                this.Summary.Append("No Diagnoses found, please include. ");
                return;
            }
            for (int i = 0; i < diagcnt*2; i+=2)
            {
                var notPresentDX = string.IsNullOrEmpty(this.pipeFileInput[startingIndex + i]);
                var notPresentPOA = string.IsNullOrEmpty(this.pipeFileInput[startingIndex + i + 1]);

                if (notPresentDX)
                {
                    this.Summary.Append("Empty DX").Append(i + 1).Append(" field found. ");
                }
                this.csv.addRow("DX" + (i + 1), !notPresentDX, "R", "Variable", this.pipeFileInput[startingIndex + i]);

                if (notPresentPOA)
                {
                    this.Summary.Append("Empty POA").Append(i + 1).Append(" field found, if the POA is available for this DX please include. ");
                }
                this.csv.addRow("POA" + (i + 1), !notPresentPOA, "RE", "Variable", this.pipeFileInput[startingIndex + i + 1]);
                
            }
        }

        private void validatePipeIPProcedures(int proccnt, int diagcnt)
        {
            var procs = this.pipeFileInput.GetRange(((int)DataReference.InpatientPipeDataLocations.PROCCNT+1+(diagcnt*2)), 3);
            if (procs == null || procs.Count == 0)
            {
                this.Summary.Append("No Procedures found, if available please include. ");
                return;
            }
            for(var i = 0; i < proccnt*3; i+=3)
            {
                var proc = i + 1;
                var px = procs[i];
                var procDate = procs[i+1];
                var procProvider = procs[i+2];

                var notPresentPX = string.IsNullOrEmpty(px);
                var notPresentProcDate = string.IsNullOrEmpty(procDate);
                var notPresentProcProvider = string.IsNullOrEmpty(procProvider);

                if (notPresentPX)
                {
                    this.Summary.Append("Empty PX").Append(i).Append(" field found, if available please include. ");
                }
                if (notPresentProcDate)
                {
                    this.Summary.Append("Empty Procedure Date (Row ").Append(i).Append(") field found, if the Procedure Date is available for this Procedure please include. ");
                }
                if (notPresentProcProvider)
                {
                    this.Summary.Append("Empty Procedure Provider (Row ").Append(i).Append(") field found, if the Procedure Provider is available for this Procedure please include. ");
                }
                this.csv.addRow("PX" + (i + 1), !notPresentPX, "RE", "Variable", px);
                this.csv.addRow("Procedure Date", !notPresentProcDate, "RE", "Variable", procDate);
                this.csv.addRow("Procedure Provider", !notPresentProcProvider, "RE", "Variable", procProvider);
            }
        }

        private void validatePipeOPDiagnoses(int diagcnt)
        {
            var startingIndex = ((int)DataReference.InpatientPipeDataLocations.DIAGCNT) + 2;
            if (startingIndex == -1)
            {
                this.Summary.Append("No Diagnoses found, please include. ");
                return;
            }
            for (int i = 0; i < diagcnt; i++)
            {
                var notPresent = string.IsNullOrEmpty(this.pipeFileInput[startingIndex + i]);
                if (notPresent)
                {
                    this.Summary.Append("Empty DX").Append(i + 1).Append(" field found. ");
                }
                this.csv.addRow("DX" + i, !notPresent, "R", "Variable", this.pipeFileInput[startingIndex + i]);
            }
        }

        private void validatePipeOPProcedures(int proccnt, int diagcnt)
        {
            var procs = this.pipeFileInput.GetRange(((int)DataReference.InpatientPipeDataLocations.PROCCNT + 1 + (diagcnt * 2)), 4);
            if (procs == null || procs.Count == 0)
            {
                this.Summary.Append("No Procedures found, if available please include. ");
                return;
            }
            for (var i = 0; i < proccnt*4; i += 4)
            {
                var proc = i + 1;
                var px = procs[i];
                var procMod = procs[i + 1];
                var procDate = procs[i + 2];
                var procProvider = procs[i + 3];

                var notPresentPX = string.IsNullOrEmpty(px);
                var notPresentProcMod = string.IsNullOrEmpty(procMod);
                var notPresentProcDate = string.IsNullOrEmpty(procDate);
                var notPresentProcProvider = string.IsNullOrEmpty(procProvider);

                if (notPresentPX)
                {
                    this.Summary.Append("Empty PX").Append(i).Append(" field found, if available please include. ");
                }
                if (notPresentProcMod)
                {
                    this.Summary.Append("Empty Procedure Modifier (Row ").Append(i).Append(") field found, if the Procedure Modifier is available for this Procedure please include. ");
                }
                if (notPresentProcDate)
                {
                    this.Summary.Append("Empty Procedure Date (Row ").Append(i).Append(") field found, if the Procedure Date is available for this Procedure please include. ");
                }
                if (notPresentProcProvider)
                {
                    this.Summary.Append("Empty Procedure Provider (Row ").Append(i).Append(") field found, if the Procedure Provider is available for this Procedure please include. ");
                }

                this.csv.addRow("PX" + i, !notPresentPX, "RE", "Variable", px);
                this.csv.addRow("Procedure Modifier", !notPresentProcDate, "RE", "Variable", procDate);
                this.csv.addRow("Procedure Date", !notPresentProcDate, "RE", "Variable", procDate);
                this.csv.addRow("Procedure Provider", !notPresentProcProvider, "RE", "Variable", procProvider);
            }
        }

        private void validatePipeOPCharges()
        {
            var chargeDate = this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.ChargeDate];
            var chargeCode = this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.ChargeCode];
            var chargeDescription = this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.ChargeDescription];
            var chargeCPTCode = this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.CPTCode];
            var chargeRevenueCode = this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.RevenueCode];
            var chargeUnitsOfService = this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.UnitsOfService];
            var chargeAmount = this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.Charges];
            var chargeAPC = this.pipeFileInput[(int)DataReference.OutpatientPipeDataLocations.APC];

            var notPresentChargeDate = string.IsNullOrEmpty(chargeDate);
            var notPresentChargeCode = string.IsNullOrEmpty(chargeCode);
            var notPresentChargeDescription = string.IsNullOrEmpty(chargeDescription);
            var notPresentChargeCPTCode = string.IsNullOrEmpty(chargeCPTCode);
            var notPresentRevenueCode = string.IsNullOrEmpty(chargeRevenueCode);
            var notPresentUnitsOfService = string.IsNullOrEmpty(chargeUnitsOfService);
            var notPresentChargeAmount = string.IsNullOrEmpty(chargeAmount);
            var notPresentAPC = string.IsNullOrEmpty(chargeAPC);

            if (notPresentChargeDate)
            {
                this.Summary.Append("CHARGE DATE").Append(DataReference.REFieldMessage).Append(DataReference.dateFormat);
            }
            if (notPresentChargeCode)
            {
                this.Summary.Append("CHARGE CODE").Append(DataReference.RFieldMessage).Append(DataReference.alphaNumericFormat);
            }
            if (notPresentChargeDescription)
            {
                this.Summary.Append("CHARGE DESCRIPTION").Append(DataReference.REFieldMessage).Append(DataReference.alphaNumericFormat);
            }
            if (notPresentChargeCPTCode)
            {
                this.Summary.Append("CHARGE CPT/HCPCS CODE").Append(DataReference.REFieldMessage).Append(DataReference.alphaNumericFormat);
            }
            if (notPresentRevenueCode)
            {
                this.Summary.Append("REVENUE CODE").Append(DataReference.REFieldMessage).Append(DataReference.numberFormat);
            }
            if (notPresentUnitsOfService)
            {
                this.Summary.Append("UNITS OF SERVICE").Append(DataReference.REFieldMessage).Append(DataReference.numberFormat);
            }
            if (notPresentChargeAmount)
            {
                this.Summary.Append("CHARGES (Amount)").Append(DataReference.RFieldMessage).Append(DataReference.moneyFormat);
            }
            if (notPresentAPC)
            {
                this.Summary.Append("APC").Append(DataReference.REFieldMessage).Append(DataReference.numberFormat);
            }

            this.csv.addRow("Charge Date", !notPresentChargeDate, "RE", ((int)DataReference.OutpatientPipeDataLocations.ChargeDate + 1).ToString(), chargeDate);
            this.csv.addRow("Charge Code", !notPresentChargeCode, "R", ((int)DataReference.OutpatientPipeDataLocations.ChargeCode + 1).ToString(), chargeCode);
            this.csv.addRow("Charge Description", !notPresentChargeDescription, "RE", ((int)DataReference.OutpatientPipeDataLocations.ChargeDescription + 1).ToString(), chargeDescription);
            this.csv.addRow("Charge CPT/HCPCS Code", !notPresentChargeCPTCode, "RE", ((int)DataReference.OutpatientPipeDataLocations.CPTCode + 1).ToString(), chargeCPTCode);
            this.csv.addRow("Charge Revenue Code", !notPresentRevenueCode, "RE", ((int)DataReference.OutpatientPipeDataLocations.RevenueCode + 1).ToString(), chargeRevenueCode);
            this.csv.addRow("Charge Units of Service", !notPresentUnitsOfService, "RE", ((int)DataReference.OutpatientPipeDataLocations.UnitsOfService + 1).ToString(), chargeUnitsOfService);
            this.csv.addRow("Charges", !notPresentChargeAmount, "R", ((int)DataReference.OutpatientPipeDataLocations.Charges + 1).ToString(), chargeAmount);
            this.csv.addRow("Charge APC", !notPresentAPC, "RE", ((int)DataReference.OutpatientPipeDataLocations.APC + 1).ToString(), chargeAPC);
        }

    }
}