﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace eValidator.Models
{
    public class OutputCSV
    {
        private StringBuilder outputStream;

        public OutputCSV()
        {
            this.outputStream = new StringBuilder();
            this.addHeaderRow();
        }

        private void addHeaderRow()
        {
            this.outputStream.Append("Data Element,").Append("Data Present,").Append("R/RE,").Append("Expected Field Location,").Append("Client Sample File Data Value").Append(Environment.NewLine);
        }
        
        public void addRow(string elementName, bool dataPresent, string requiredStatus, string fieldLocation, string clientDataValue)
        {
            var boolString = "";
            if (dataPresent)
            {
                boolString = "X";
            }
            this.outputStream.Append(elementName).Append(",").Append(boolString).Append(",").Append(requiredStatus).Append(",").Append(fieldLocation).Append(",").Append(clientDataValue).Append(Environment.NewLine);
        }

        public string getOutputStream()
        {
            return this.outputStream.ToString();
        }
    }
}