﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eValidator.Models
{
    public static class DataReference
    {
        public static string REFieldMessage = " is missing, if available please include in the following format: ";
        public static string RFieldMessage = " is missing, please include in the following format: ";
        public static string RFieldNoFormatMessage = " is missing, please include.";
        public static string dateFormat = "mm/dd/yyyy hh:mm:ss. ";
        public static string nameFormat = "Lastname,Firstname. ";
        public static string twoDigitFormat = "2 digit number. ";
        public static string numberFormat = "a string of numbers. ";
        public static string alphaNumericFormat = "a string of alphanumeric characters. ";
        public static string alphaNumericWithSpecialCharFormat = "a string of alphanumeric characters with special characters allowed. ";
        public static string sexFormat = "Single character to denote gender. ";
        public static string moneyFormat = "$123.45. ";
        public static string singleNumericFormat = "a single numeric character. ";

        //Dummy Data
        public static string dummyFacility = "AMERICA";
        public static string dummyMRN = "123456";
        public static string dummyPatientName = "\"DOE,JOHN\"";
        public static string dummyDate = "12/31/1998 00:00";
        public static string dummyAccount = "123123";

        //HL7 Data Locations
        public static string expectedFacilityLocation = "MSH.4";
        public static string expectedAccountLocation = "PID.18";
        public static string expectedMRNLocation = "PID.3";
        public static string expectedPatientNameLocation = "PID.5";
        public static string expectedAdmitDateLocation = "PV1.44";
        public static string expectedDischargeDateLocation = "PV1.45";
        public static string expectedDISCHDISPLocation = "PV1.36";
        public static string expectedDOBLocation = "PID.7";
        public static string expectedSexLocation = "PID.8";
        public static string expectedServiceLocation = "PV1.10";
        public static string expectedPayorLocation = "PV1.20";
        public static string expectedTotalChargesLocation = "PV1.47";
        public static string expectedAttendingNameLocation = "PV1.7";
        public static string expectedCoderLocation = "EVN.5.1";
        public static string expectedProceduresLocation = "PR1";
        public static string expectedDRGLocation = "DRG.1";
        public static string expectedSOILocation = "DRG.15";
        public static string expectedROMLocation = "DRG.14";
        public static string expectedDiagnosesLocation = "DG1";
        public static string expectedOPChargesLocation = "FT1";

        //Pipe Delimited Data Locations/Indexes
        public enum InpatientPipeDataLocations
        {
            Facility,
            Account,
            MRN,
            PatientName,
            Admit,
            Discharge,
            DischDisp,
            DOB,
            Sex,
            Service,
            Payor,
            Charges,
            AttendingName,
            Coder,
            MSDRG,
            APRDRG,
            ROM,
            SOI,
            DIAGCNT,
            PROCCNT
        }

        public enum OutpatientPipeDataLocations
        {
            Account,
            MRN,
            PatientName,
            Admit,
            Discharge,
            DischDisp,
            DOB,
            Sex,
            Service,
            Payor,
            AttendingName,
            Coder,
            ChargeDate,
            ChargeCode,
            ChargeDescription,
            CPTCode,
            RevenueCode,
            UnitsOfService,
            Charges,
            APC,
            DIAGCNT,
            PROCCNT
        }
    }
}