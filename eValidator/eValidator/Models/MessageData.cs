﻿using HL7.Dotnetcore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static eValidator.Models.DataElement;

namespace eValidator.Models
{
    public abstract class MessageData
    {
        public List<DataElement> dataElementsInpatient;
        public List<DataElement> dataElementsOutpatient;

        protected MessageData(Message msg)
        {
            this.dataElementsInpatient = new List<DataElement>();
            this.dataElementsOutpatient = new List<DataElement>();
            this.addToBothLists("FACILITY", RequiredValues.R, msg.GetValue("MSH.4"));
            this.addToBothLists("ACCOUNT", RequiredValues.R, msg.GetValue("PID.18"));
            this.addToBothLists("MRN", RequiredValues.R, msg.GetValue("PID.3"));
            this.addToBothLists("PATIENTNAME", RequiredValues.RE, msg.GetValue("PID.5"));
            this.addToBothLists("ADMIT", RequiredValues.RE, msg.GetValue("PV1.44"));
            this.addToBothLists("DISCHARGE", RequiredValues.RE, msg.GetValue("PV1.45"));
            this.addToBothLists("DISCHDISP", RequiredValues.RE, msg.GetValue("PV1.36"));
            this.addToBothLists("DOB", RequiredValues.RE, msg.GetValue("PID.7"));
            this.addToBothLists("SEX", RequiredValues.RE, msg.GetValue("PID.8"));
            this.addToBothLists("SERVICE", RequiredValues.R, msg.GetValue("PV1.10"));
            this.addToBothLists("PAYOR", RequiredValues.R, msg.GetValue("PV1.20"));
            this.addToBothLists("ATTENDINGNAME", RequiredValues.RE, msg.GetValue("PV1.7"));
            this.addToBothLists("CODER", RequiredValues.RE, msg.GetValue("PV1.7"));
        }

        private void addToBothLists(string name, RequiredValues req, string value)
        {
            this.addToIP(name, req, value);
            this.addToOP(name, req, value);
        }

        private void addToIP(string name, RequiredValues req, string value)
        {
            var newElement = new DataElement(name, req, value);
            this.dataElementsInpatient.Add(newElement);
        }

        private void addToOP(string name, RequiredValues req, string value)
        {
            var newElement = new DataElement(name, req, value);
            this.dataElementsOutpatient.Add(newElement);
        }
    }
}