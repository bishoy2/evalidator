﻿var fileContent = "";

function callHL7ProcessAjax() {
    $.ajax({
        url: '/Home/processFile',
        data: { rawMessage: fileContent, messageType: $("input[name=messageTypeRadio]:checked").val(), patientType: $("input[name=patientTypeRadio]:checked").val(), deidentify: $("#deidentify").is(':checked') },
        type: 'POST',
        dataType: "text",
        success: function (result) {
            if (result != undefined) {
                var results = result.split(". ");
                $("#feedback").empty();
                for (var i = 0; i < results.length; i++) {
                    $("#feedback").append("<p>" + results[i] + "</p>");
                }
                var receive = $("#receiveOutputFile:checked").val();
                if (receive) {
                    getOutputCSVContent();
                }
            }
        }
    });
}

function getOutputCSVContent() {
    $.ajax({
        url: '/Home/getCSVString',
        data: { rawMessage: fileContent, messageType: $("input[name=messageTypeRadio]:checked").val(), patientType: $("input[name=patientTypeRadio]:checked").val(), deidentify: $("#deidentify").is(':checked') },
        type: 'POST',
        dataType: "text",
        success: function (result) {
            if (result != undefined) {
                downloadFile("eValidatorDiagnostic.csv", result);
            }
        }
    });
}

function handleFileSelect() {
    if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
        alert('The File APIs are not fully supported in this browser. Please use Google Chrome.');
        return;
    }

    input = document.getElementById('HL7InputFile');
    if (!input) {
        alert("Couldn't find the fileinput element.");
    } else if (!input.files) {
        alert("This browser doesn't seem to support the `files` property of file inputs. Please use Google Chrome.");
    } else if (!input.files[0]) {
        alert("Please select a file before clicking 'Process Data'");
    } else {
        file = input.files[0];  
        fr = new FileReader();
        fr.onload = receivedText;
        fr.readAsText(file);
    }
}

function receivedText() {
    fileContent = fr.result;
    callHL7ProcessAjax();
}     

function downloadFile(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}